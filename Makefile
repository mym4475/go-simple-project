#!/bin/bash

.PHONY: build
build: binary

.PHONY: binary
binary:
	@echo "Building binary..."
	@go build -tags static_all .

.PHONY: clean
clean:
	@echo "Cleaning..."
	@rm -f go-simple-boilerplate
	@rm -rf vendor
	@rm -f go.sum

.PHONY: install
install:
	@echo "Installing dependencies..."
	@rm -rf vendor
	@rm -f Gopkg.lock
	@rm -f glide.lock
	@go mod tidy && go mod download && go mod vendor

.PHONY: test
test:
	@go test $$(go list ./... | grep -v /vendor/) -cover

.PHONY: test-cover
test-cover:
	@go test $$(go list ./... | grep -v /vendor/) -coverprofile=cover.out && go tool cover -html=cover.out ; rm -f cover.out

.PHONY: coverage
coverage:
	@go test -covermode=count -coverprofile=count.out fmt; rm -f count.out

.PHONY: start
start:
	@go run main.go http

.PHONY: format
format:
	@go fmt $$(go list ./... | grep -v /vendor/)
