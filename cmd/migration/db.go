// Package migration
package migration

import (
	"go-simple-boilerplate/internal/appctx"
	"go-simple-boilerplate/pkg/databasex"
)

func MigrateDatabase() {
	cfg := appctx.NewConfig()

	databasex.DatabaseMigration(&databasex.Config{
		Driver:       cfg.DB.Driver,
		Host:         cfg.DB.Host,
		Port:         cfg.DB.Port,
		Name:         cfg.DB.Name,
		User:         cfg.DB.User,
		Password:     cfg.DB.Pass,
		Charset:      cfg.DB.Charset,
		DialTimeout:  cfg.DB.DialTimeout,
		MaxIdleConns: cfg.DB.MaxIdle,
		MaxOpenConns: cfg.DB.MaxOpen,
		MaxLifetime:  cfg.DB.MaxLifeTime,
		TimeZone:     cfg.DB.Timezone,
	})
}
