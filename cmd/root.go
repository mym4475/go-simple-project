package cmd

import (
	"context"
	"go-simple-boilerplate/cmd/http"
	"go-simple-boilerplate/cmd/migration"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
)

// Start handler registering service command
func Start() {
	rootCmd := &cobra.Command{}

	ctx, cancel := context.WithCancel(context.Background())

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-quit
		cancel()
	}()

	migrateCmd := &cobra.Command{
		Use:   "db:migrate",
		Short: "Database migration",
		Run: func(c *cobra.Command, args []string) {
			migration.MigrateDatabase()
		},
	}

	migrateCmd.Flags().BoolP("version", "", false, "print version")
	migrateCmd.Flags().StringP("dir", "", "database/migration/", "directory with migration files")
	migrateCmd.Flags().StringP("table", "", "db_migration", "migrations table name")
	migrateCmd.Flags().BoolP("verbose", "", false, "enable verbose mode")
	migrateCmd.Flags().BoolP("guide", "", false, "print help")
	migrateCmd.Flags().StringP("dsn", "", "", "database dsn")

	cmd := []*cobra.Command{
		{
			Use:   "http",
			Short: "Run HTTP server",
			Run: func(cmd *cobra.Command, args []string) {
				http.Start(ctx)
			},
		},
		migrateCmd,
	}

	rootCmd.AddCommand(cmd...)
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
