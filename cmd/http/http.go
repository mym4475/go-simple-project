package http

import (
	"context"
	"go-simple-boilerplate/internal/server"
	"log"
)

// Start starting http listener
func Start(ctx context.Context) {
	serve := server.NewHTTPServer()
	defer serve.Done()

	log.Printf("starting %s services... %d", serve.Config().App.AppName, serve.Config().App.Port)

	if err := serve.Run(ctx); err != nil {
		log.Printf("service stopped, got %v", err)
	}
}
