# go-simple-boilerplate

go-simple-boilerplate

## Running on your local machine

Linux or MacOS

### Installation guide

1. #### Install Go version 1.21 or latest

    Please read this link installation guide of go <https://golang.org/doc/install>

2. #### Build the application

    Run command:

    ```bash
    cd $HOME/go/src/go-simple-boilerplate
    git clone -b development https://go-simple-boilerplate .
    cd $HOME/go/src/go-simple-boilerplate
    go mod tidy && go mod download && go mod vendor
    cp config/app.yaml.dist config/app.yaml
    ```

    Edit config/app.yaml with the server environment
    then build the app

    ```bash
    go build
    ```

    Run the application after build or create on *supervisord*

    ```bash
    ./go-simple-boilerplate http
    ```

3. #### Health check route path

    ```http request
    GET {{host}}/liveness
    ```

## Database Migration

Migration up

```bash
go run main.go db:migrate up

# using custom dsn
go run main.go db:migrate --dsn="user:password@tcp(localhost:3306)/dbname?parseTime=true" up
```

Migration down

```bash
go run main.go db:migrate down

# using custom dsn
go run main.go db:migrate --dsn="user:password@tcp(localhost:3306)/dbname?parseTime=true" down
```

Migration reset

```bash
go run main.go db:migrate reset

# using custom dsn
go run main.go db:migrate --dsn="user:password@tcp(localhost:3306)/dbname?parseTime=true" reset
```

Migration redo

```bash
go run main.go db:migrate redo

# using custom dsn
go run main.go db:migrate --dsn="user:password@tcp(localhost:3306)/dbname?parseTime=true" redo
```

Migration status

```bash
go run main.go db:migrate status

# using custom dsn
go run main.go db:migrate --dsn="user:password@tcp(localhost:3306)/dbname?parseTime=true" status
```

Create migration table

```bash
go run main.go db:migrate create {tableName} sql

# example
go run main.go db:migrate create users sql
```

To show all command

```bash
go run main.go db:migrate
```
