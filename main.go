// Package main
package main

import (
	"go-simple-boilerplate/cmd"
)

func main() {
	cmd.Start()
}
