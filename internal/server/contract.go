// Package server
package server

import (
	"context"
	"go-simple-boilerplate/internal/appctx"
	"net/http"

	ucase "go-simple-boilerplate/internal/ucase/contract"
)

// httpHandlerFunc abstraction for http handler
type httpHandlerFunc func(req *http.Request, svc ucase.UseCase, cfg *appctx.Config) appctx.Response

// Server contract
type Server interface {
	Run(context.Context) error
	Done()
	Config() *appctx.Config
}
