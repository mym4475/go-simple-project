// Package server
package server

import (
	"context"
	"fmt"
	"go-simple-boilerplate/internal/appctx"
	"go-simple-boilerplate/internal/router"
	"log"
	"net/http"
	"time"
)

// NewHTTPServer creates http server instance
// returns: Server instance
func NewHTTPServer() Server {
	cfg := appctx.NewConfig()
	return &httpServer{
		config: cfg,
		router: router.NewRouter(cfg),
	}
}

// httpServer as HTTP server implementation
type httpServer struct {
	config *appctx.Config
	router router.Router
}

// Run runs the http server gracefully
// returns:
//
//	err: error operation
func (h *httpServer) Run(ctx context.Context) error {
	var err error

	server := http.Server{
		Addr:         fmt.Sprintf("0.0.0.0:%d", h.config.App.Port),
		Handler:      h.router.Route(),
		ReadTimeout:  h.config.App.ReadTimeout,
		WriteTimeout: h.config.App.WriteTimeout,
	}

	go func() {
		err = server.ListenAndServe()
		if err != http.ErrServerClosed {
			log.Printf("http server got %v", err)
		}
	}()

	<-ctx.Done()

	shutDownCtx, cancel := context.WithTimeout(context.Background(), 6*time.Second)
	defer func() {
		cancel()
	}()

	if err = server.Shutdown(shutDownCtx); err != nil {
		log.Fatalf("failure in server shutdown: %v", err)
	}

	log.Print("server exited properly")

	if err == http.ErrServerClosed {
		err = nil
	}

	return err
}

// Done runs event wheen service stopped
func (h *httpServer) Done() {
	log.Print("service http stopped")
}

// Config  func to handle get config will return Config object
func (h *httpServer) Config() *appctx.Config {
	return h.config
}
