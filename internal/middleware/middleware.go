// Package middleware
package middleware

import (
	"go-simple-boilerplate/internal/appctx"
	"net/http"
)

// MiddlewareFunc is contract for middleware and must implement this type for http if need middleware http request
type MiddlewareFunc func(w http.ResponseWriter, r *http.Request, cfg *appctx.Config) bool

// FilterFunc is an iterator resolver in each middleware registered
func FilterFunc(w http.ResponseWriter, r *http.Request, cfg *appctx.Config, mfs []MiddlewareFunc) bool {
	for _, mf := range mfs {
		if ok := mf(w, r, cfg); !ok {
			return false
		}
	}

	return true
}
