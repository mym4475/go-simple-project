package middleware

import (
	"go-simple-boilerplate/internal/appctx"
	"go-simple-boilerplate/internal/consts"
	"net/http"
)

func NotFound(w http.ResponseWriter, r *http.Request) {
	rsp := appctx.NewResponse().
		WithMsgKey(consts.RespNoRouteFound).
		Generate()
	w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
	w.WriteHeader(rsp.Code)
	w.Write(rsp.Byte())
}
