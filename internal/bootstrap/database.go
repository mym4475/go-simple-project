// Package bootstrap
package bootstrap

import (
	"go-simple-boilerplate/internal/appctx"
	"go-simple-boilerplate/pkg/databasex"
	"log"
)

// RegistryDatabase initialize database session
func RegistryDatabase(cfg *appctx.Database) *databasex.DB {
	db, err := databasex.CreateSession(&databasex.Config{
		Driver:       cfg.Driver,
		Host:         cfg.Host,
		Name:         cfg.Name,
		Password:     cfg.Pass,
		Port:         cfg.Port,
		User:         cfg.User,
		DialTimeout:  cfg.DialTimeout,
		MaxOpenConns: cfg.MaxOpen,
		MaxIdleConns: cfg.MaxIdle,
		MaxLifetime:  cfg.MaxLifeTime,
		Charset:      cfg.Charset,
		TimeZone:     cfg.Timezone,
	})
	if err != nil {
		log.Fatalf("registry db, host %v, port %v, driver %v, timezone %v, error %v",
			cfg.Host, cfg.Port, cfg.Driver, cfg.Timezone, err)
	}

	return databasex.New(db, false, cfg.Name)
}
