// Package bootstrap
package bootstrap

import (
	"go-simple-boilerplate/internal/consts"
	"go-simple-boilerplate/pkg/msgx"
	"log"
)

func RegistryMessage() {
	err := msgx.Setup("msg.yaml", consts.ConfigPath)
	if err != nil {
		log.Fatalf("file message multi language load error %v", err)
	}
}
