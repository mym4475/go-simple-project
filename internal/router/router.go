// Package router
package router

import (
	"go-simple-boilerplate/internal/appctx"
	"go-simple-boilerplate/internal/bootstrap"
	"go-simple-boilerplate/internal/consts"
	"go-simple-boilerplate/internal/handler"
	"go-simple-boilerplate/internal/middleware"
	"go-simple-boilerplate/internal/ucase"
	"go-simple-boilerplate/pkg/msgx"
	"go-simple-boilerplate/pkg/routerkit"
	"log"
	"net/http"
	"runtime/debug"

	ucaseContract "go-simple-boilerplate/internal/ucase/contract"
)

// router object contract
type router struct {
	config *appctx.Config
	router *routerkit.Router
}

// NewRouter initialize new router
func NewRouter(cfg *appctx.Config) Router {
	bootstrap.RegistryMessage()

	return &router{
		config: cfg,
		router: routerkit.NewRouter(routerkit.WithServiceName(cfg.App.AppName)),
	}
}

// handle dispatch any middleware, including language setting,
// error handling, init context, etc.
func (rtr *router) handle(hfn httpHandlerFunc, svc ucaseContract.UseCase, mdws ...middleware.MiddlewareFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		lang := r.Header.Get(consts.HeaderLanguageKey)
		if !msgx.HaveLang(consts.RespOK, lang) {
			lang = rtr.config.App.DefaultLang
			r.Header.Set(consts.HeaderLanguageKey, lang)
		}

		defer func() {
			err := recover()
			if err != nil {
				w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
				rsp := appctx.NewResponse().
					WithCode(http.StatusInternalServerError)
				rsp.WithLang(lang)

				w.WriteHeader(rsp.Code)
				w.Write(rsp.Byte())

				log.Printf("error %v", string(debug.Stack()))
				return
			}
		}()

		// validate middleware
		if !middleware.FilterFunc(w, r, rtr.config, mdws) {
			return
		}

		rsp := hfn(r, svc, rtr.config)

		rsp.WithLang(lang)
		rtr.response(w, rsp)
	}
}

// response prints as a json and formatted string for DGP legacy
func (rtr *router) response(w http.ResponseWriter, rsp appctx.Response) {
	w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
	rsp.Generate()
	w.WriteHeader(rsp.Code)
	w.Write(rsp.Byte())
}

// Route preparing http router and will return mux router object
func (rtr *router) Route() *routerkit.Router {
	rtr.router.NotFoundHandler = http.HandlerFunc(middleware.NotFound)

	root := rtr.router.PathPrefix("/").Subrouter()
	liveness := root.PathPrefix("/").Subrouter()

	// create session database for single database
	//db := bootstrap.RegistryDatabase(rtr.config.WriteDB)

	// use case
	healthy := ucase.NewHealthCheck()

	// healthy
	liveness.HandleFunc("/liveness", rtr.handle(
		handler.HttpRequest,
		healthy,
	)).Methods(http.MethodGet)

	return rtr.router
}
