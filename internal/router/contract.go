// Package router
package router

import (
	"go-simple-boilerplate/internal/appctx"
	"go-simple-boilerplate/internal/ucase/contract"
	"go-simple-boilerplate/pkg/routerkit"
	"net/http"
)

// httpHandlerFunc is a contract http handler for router
type httpHandlerFunc func(req *http.Request, svc contract.UseCase, cfg *appctx.Config) appctx.Response

// Router is a contract router and must implement this interface
type Router interface {
	Route() *routerkit.Router
}
