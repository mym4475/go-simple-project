// Package contract
package contract

import (
	"go-simple-boilerplate/internal/appctx"
)

// UseCase is a use case contract
type UseCase interface {
	Serve(data *appctx.Data) appctx.Response
}
