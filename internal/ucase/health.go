package ucase

import (
	"go-simple-boilerplate/internal/appctx"
	"go-simple-boilerplate/internal/ucase/contract"
	"net/http"
)

type healthCheck struct{}

func NewHealthCheck() contract.UseCase {
	return &healthCheck{}
}

func (u *healthCheck) Serve(*appctx.Data) appctx.Response {
	return *appctx.NewResponse().WithCode(http.StatusOK).WithMessage("ok")
}
