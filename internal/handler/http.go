// Package handler
package handler

import (
	"go-simple-boilerplate/internal/appctx"
	"go-simple-boilerplate/internal/ucase/contract"
	"net/http"
)

// HttpRequest handler func wrapper
func HttpRequest(r *http.Request, svc contract.UseCase, cfg *appctx.Config) appctx.Response {
	data := &appctx.Data{
		Request:     r,
		Config:      cfg,
		ServiceType: "http",
	}

	return svc.Serve(data)
}
