// Package appctx
package appctx

import (
	"net/http"
)

// Data context for http use case
type Data struct {
	Request     *http.Request
	Config      *Config
	ServiceType string
}
