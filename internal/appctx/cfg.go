// Package appctx
package appctx

import (
	"fmt"
	"log"
	"sync"
	"time"

	"go-simple-boilerplate/internal/consts"
	"go-simple-boilerplate/pkg/file"
)

var (
	oneCfg  *Config
	cfgOnce sync.Once
)

// NewConfig initialize config object
func NewConfig() *Config {
	paths := []string{consts.ConfigPath}
	cfgOnce.Do(func() {
		cfg, err := readCfg("app.yaml", paths...)
		if err != nil {
			log.Fatal(err)
		}

		oneCfg = cfg
	})

	return oneCfg
}

// Config object contract
//
//go:generate easytags $GOFILE yaml,json
type Config struct {
	App *Common   `yaml:"app" json:"app"`
	DB  *Database `yaml:"db" json:"db"`
}

// Common general config object contract
type Common struct {
	AppName      string        `yaml:"name" json:"name"`
	ApiKey       string        `yaml:"key" json:"api_key"`
	Debug        bool          `yaml:"debug" json:"debug"`
	Maintenance  bool          `yaml:"maintenance" json:"maintenance"`
	Timezone     string        `yaml:"timezone" json:"timezone"`
	Env          string        `yaml:"env" json:"env"`
	Port         int           `yaml:"port" json:"port"`
	ReadTimeout  time.Duration `yaml:"read_timeout_second" json:"read_timeout"`
	WriteTimeout time.Duration `yaml:"write_timeout" json:"write_timeout"`
	DefaultLang  string        `yaml:"default_lang" json:"default_lang"`
}

// Database configuration structure
type Database struct {
	Name         string        `yaml:"name" json:"name"`
	User         string        `yaml:"user" json:"user"`
	Pass         string        `yaml:"pass" json:"pass"`
	Host         string        `yaml:"host" json:"host"`
	Port         int           `yaml:"port" json:"port"`
	MaxOpen      int           `yaml:"max_open" json:"max_open"`
	MaxIdle      int           `yaml:"max_idle" json:"max_idle"`
	DialTimeout  time.Duration `yaml:"dial_timeout" json:"dial_timeout"`
	MaxLifeTime  time.Duration `yaml:"life_time" json:"life_time"`
	ReadTimeout  time.Duration `yaml:"read_timeout" json:"read_timeout"`
	WriteTimeout time.Duration `yaml:"write_timeout" json:"write_timeout"`
	Charset      string        `yaml:"charset" json:"charset"`
	Driver       string        `yaml:"driver" json:"driver"`
	Timezone     string        `yaml:"timezone" json:"timezone"`
}

// readCfg reads the configuration from file
//
// args:
//   - fName: filename
//   - paths: full path of possible configuration files
//
// returns:
//   - *config.Config: configuration pointer object
//   - error: error operation
func readCfg(fName string, paths ...string) (*Config, error) {
	var cfg *Config
	var errs []error

	for _, p := range paths {
		f := fmt.Sprint(p, fName)

		err := file.ReadFromYAML(f, &cfg)
		if err != nil {
			errs = append(errs, fmt.Errorf("file %s error %s", f, err.Error()))
			continue
		}
		break
	}

	if cfg == nil {
		return nil, fmt.Errorf("file config parse error %v", errs)
	}

	return cfg, nil
}
