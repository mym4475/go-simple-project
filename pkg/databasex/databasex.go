package databasex

import "time"

const (
	connStringMySQLTemplate    = "%s:%s@tcp(%s:%d)/%s?%s"
	connStringPostgresTemplate = "postgres://%s/%s?%s"
)

var dsn = map[string]func(*Config) string{
	"mysql":    mysqlDSN,
	"postgres": postgresDSN,
}

type (
	// Config database
	Config struct {
		Host         string
		Port         int
		User         string
		Password     string
		Name         string
		Charset      string
		MaxOpenConns int
		MaxIdleConns int
		MaxLifetime  time.Duration
		Type         string
		TimeZone     string
		Driver       string
		DialTimeout  time.Duration
		ReadTimeout  time.Duration
		WriteTimeout time.Duration
	}
)
