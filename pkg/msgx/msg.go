// Package msgx
// @author Daud Valentino
package msgx

import (
	"fmt"
	"go-simple-boilerplate/pkg/file"
	"strings"
	"sync"
)

var (
	once       sync.Once
	oneMsg     map[string]*Message
	defaultMsg = msgLang{
		text:   "unknown",
		lang:   "en",
		status: 520,
	}
)

// Message contract
type Message struct {
	Name   string `yaml:"name"`
	Status int    `yaml:"status"`
	Langs  []Lang `yaml:"message"`
	lang   map[string]*msgLang
}

// doMap create content map from slice
func (m *Message) doMap() *Message {
	m.lang = make(map[string]*msgLang, 0)
	for _, c := range m.Langs {
		l := strings.ToLower(c.Lang)
		if _, ok := m.lang[l]; !ok {
			m.lang[l] = &msgLang{lang: c.Lang, status: m.Status, text: c.Text}
			continue
		}
	}

	return m
}

// Lang language setting
type Lang struct {
	Lang string `yaml:"lang"`
	Text string `yaml:"text"`
}

// msgLang language message setting
type msgLang struct {
	status int
	lang   string
	text   string
}

func (p msgLang) Text() string {
	return p.text
}

func (p msgLang) Status() int {
	return p.status
}

func (p msgLang) Lang() string {
	return p.lang
}

func Setup(fName string, paths ...string) error {
	var (
		err  error
		msgs []Message
	)

	once.Do(func() {
		oneMsg = make(map[string]*Message, 0)
		for _, p := range paths {
			f := fmt.Sprint(p, fName)
			err := file.ReadFromYAML(f, &msgs)
			if err != nil {
				continue
			}
			err = nil
		}
	})

	if err != nil {
		return fmt.Errorf("unable to read config from files %w", err)
	}

	for _, v := range msgs {
		if _, ok := oneMsg[v.Name]; !ok {
			m := &Message{Name: v.Name, Status: v.Status, Langs: v.Langs}
			oneMsg[m.Name] = m.doMap()
		}
	}

	return err
}

func cleanLangStr(s string) string {
	return strings.ToLower(strings.Trim(s, " "))
}

func Get(key string, lang string) msgLang {
	lang = cleanLangStr(lang)
	if m, ok := oneMsg[key]; ok {
		if v, ok := m.lang[lang]; ok {
			return *v
		}

		return msgLang{status: m.Status}
	}

	return defaultMsg
}

func HaveLang(key string, lang string) bool {
	if m, ok := oneMsg[key]; ok {
		if _, ok := m.lang[lang]; ok {
			return true
		}
	}

	return false
}
