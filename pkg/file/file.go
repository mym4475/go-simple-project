package file

import (
	"os"

	"gopkg.in/yaml.v3"
)

// ReadFromYAML reads the YAML file and pass to the object
//
// args:
//   - path: file path location
//   - target: object which will hold the value
//
// returns:
//   - error: operation state error
func ReadFromYAML(path string, target any) error {
	yf, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(yf, target)
}
